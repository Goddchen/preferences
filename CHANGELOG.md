## [1.0.8] - 05.01.2019

* Fixed Issue with PreferenceHider

## [1.0.7] - 05.01.2019

* Fixed Issue with RadioPreference widgets on subpage

## [1.0.5] - 05.01.2019

* DropdownPreference Displayed Values can now be different from saved values
* PreferencePageLink Page Title can now be different from Link-Label

## [1.0.3] - 01.01.2019

* You can now init PrefService again with a different prefix

## [1.0.0] - 31.12.2018

* Initial release.
